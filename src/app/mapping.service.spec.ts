/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MappingService } from './mapping.service';

describe('MappingService', () => {
  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [MappingService]
    });


    let store = {};


    const m  = {};
    m['-durch-'] =  11293;
    store['SEWOBE_MAPPING_JSON3'] = JSON.stringify(m);



  spyOn(localStorage, 'getItem').and.callFake(function (key) {
    //console.log('Calling GetItem on localStorage key = ' + key +"\n"+store[key]);
    return store[key];
  });
  spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
    //console.log('Calling setItem on localStorage key = ' + key);
    return store[key] = value + '';
  });
  spyOn(localStorage, 'clear').and.callFake(function () {
      store = {};
  });
  });

  it('should ...', inject([MappingService], (service: MappingService) => {
    expect(service).toBeTruthy();
  }));

  it('should save to localStorage', inject([MappingService], (service: MappingService) => {
    localStorage.setItem('KEY', 'VALUE');
    expect(localStorage.getItem('KEY')).toBe('VALUE');
  }));

  it('should restore from localStorage', inject([MappingService], (service: MappingService) => {
    expect(service.getMappings().size === 1).toBeTruthy;
    //service.getMappings().forEach((v, k, p) => {
      //console.log(v);
      //console.log(k);
      //console.log(p);
    //});
    expect(service.getMappings().get('-durch-') === 11293).toBeTruthy;
  }));

  it('should add correctly and persist to localStorage', inject([MappingService], (service: MappingService) => {
    service.addMapping('klaus', 25293);
    expect((JSON.parse(localStorage.getItem('SEWOBE_MAPPING_JSON3')) )['klaus'] === 25293).toBeTruthy;
    expect((JSON.parse(localStorage.getItem('SEWOBE_MAPPING_JSON3')) )['-durch-'] === 11293).toBeTruthy;
    expect(service.getMappings().get('klaus')).toBe(25293);
  }));
});
