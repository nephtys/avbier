import { Injectable } from '@angular/core';
import {Http, RequestOptionsArgs, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { ConfigService } from 'app/config.service';
import { SewobeUser } from 'app/sewobe-user';
import { SewobeConfig } from './sewobe-config';
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export const MOCK_JSONSTRING = `
{
"1":{"DATENSATZ":{"NR":"11293","BUCH":"","VORNAME-PRIVATPERSON":"Frederik",
"NACHNAME-PRIVATPERSON":"Reiche","UNTERKATEGORIE":"Steganlieger","HAUPTKATEGORIE":"Mieter",
"AMT":"","BIERNAME":"durch","BIERMAILCOMPATIBLE":"","EMAIL":"some@mail.de","HUETTENVERTEILER":"",
"ABBBUCHUNGSERLAUBNIS":"1","HUETTENADRESSEAKTIV":"0","AVH-MAILINGLISTE":"0"}},
"2":{"DATENSATZ":{"NR":"11294","BUCH":"","VORNAME-PRIVATPERSON":"Luca","NACHNAME-PRIVATPERSON":
"Gagligardi","UNTERKATEGORIE":"Steganlieger","HAUPTKATEGORIE":"Mieter","AMT":"","BIERNAME":"",
"BIERMAILCOMPATIBLE":"","EMAIL":"other@gmx.de","HUETTENVERTEILER":"","ABBBUCHUNGSERLAUBNIS":"0",
"HUETTENADRESSEAKTIV":"0","AVH-MAILINGLISTE":"0"}},
"3":{"DATENSATZ":{"NR":"10831","BUCH":"","VORNAME-PRIVATPERSON":"Christopher","NACHNAME-PRIVATPERSON":"Kaag"
,"UNTERKATEGORIE":"Aktivitas","HAUPTKATEGORIE":"aktiv Ka","AMT":"","BIERNAME":"Gruin","BIERMAILCOMPATIBLE":""
,"EMAIL":"","HUETTENVERTEILER":"","ABBBUCHUNGSERLAUBNIS":"0","HUETTENADRESSEAKTIV":"0","AVH-MAILINGLISTE":"0"}}
} `;


export function parseJsonObject(json: Object): SewobeUser[] {
  const nrs: Map<number, boolean> = new Map<number, boolean>();


    let i = 1;
    const arr: SewobeUser[] = [];
    while (json['' + i] != null) {
      const e = new SewobeUser();

      if ( json['' + i]['DATENSATZ'] == null) {
        i++;
        continue;
      }

      const x = json['' + i]['DATENSATZ'];

      const nn = x['NACHNAME-PRIVATPERSON'];
      if (nn != null) {
        e.nachname = nn;
      } else {
        e.nachname = '';
      }

      const vn = x['VORNAME-PRIVATPERSON'];
      if (vn != null) {
        e.vorname = vn;
      } else {
        e.vorname = '';
      }
      const bn = x['BIERNAME'];
      if (bn != null) {
        e.biername = bn;
      } else {
        e.biername = '';
      }

      const nr = x['NR'];
      if (nr != null) {
        e.mitgliedernummer = parseInt(nr, 10);
      } else {
        e.mitgliedernummer = 0;
      }

      const ls = x['ABBBUCHUNGSERLAUBNIS'];
      if (ls != null) {
        e.lastschrift = ls === '1';
      } else {
        e.lastschrift = false;
      }
      e.similarity = -1.0;

      if (!nrs.has(e.mitgliedernummer)) {
      arr.push(e);
      nrs.set(e.mitgliedernummer, true);
    } else  {
      const otherIdx = arr.findIndex(k => k.mitgliedernummer === e.mitgliedernummer);
      const other = arr[otherIdx];
      if (other != null && other.biername.trim().length === 0) {
        arr[otherIdx] = e; // this is needed if someone appears multiple time and the data is not equivalent
      }
    }
      i++;
    }
    return arr;
  }


export const MOCK_PARSED: SewobeUser[] = parseJsonObject(JSON.parse(MOCK_JSONSTRING));

export const No_Connection_Error = 'No Connection to Sewobe';

export const Local_Storage_Sewobe_Cache_Key = 'SEWOBE_RAW_DATA';

export function returnFromLocalStorage(): SewobeUser[] {
  const json = localStorage.getItem(Local_Storage_Sewobe_Cache_Key);
  if (json == null) {
    return null;
  }
  const arr = JSON.parse(json) as SewobeUser[];
  for (const e of arr) {
    Object.setPrototypeOf(e, SewobeUser.prototype);
  }
  return arr;
}

export function storeToLocalStorageAndReturn(arr: SewobeUser[]): SewobeUser[] {
  console.log('Storing Sewobe Data in LocalStorage 1');
  localStorage.setItem(Local_Storage_Sewobe_Cache_Key, JSON.stringify(arr));
  console.log('Storing Sewobe Data in LocalStorage 2');
  return arr;
}

@Injectable()
export class SewobeService {


  readonly usersFromSewobe: Observable<SewobeUser[]>
   // = new BehaviorSubject < SewobeUser[]>([])
   ;


  constructor(
    private configService: ConfigService,
    private http: Http) {
      if (configService != null && http != null) {
    this.usersFromSewobe = configService.getConfig().flatMap(config => {
    console.log('SewobeConfig used for call:');
    console.log(config);

    const requestBody: string = 'USERNAME=' + config.sewobeUsername + '&PASSWORT='
    + config.sewobePasswort + '&AUSWERTUNG_ID=' + config.sewobeAbfrageNr;

    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(config.sewobeUrl, requestBody, {
      headers: headers
    })
    .map((a, b) => storeToLocalStorageAndReturn(parseJsonObject(a.json()))) //  write to LocalStorage
  }).retry(3).catch(e => {
    console.log('Catching call ' + e);
    const arr = returnFromLocalStorage();
    if (arr != null) {
      console.log('arr from LocalStorage:');
      console.log(arr);
      return Observable.of(arr);
    } else {
      console.log('LocalStorage also contains no sewobe list');
      return Observable.throw(No_Connection_Error);
    }}
    ).share().shareReplay(); // check if still error and return cache
      }
}


}

