import {SewobeUser} from './sewobe-user';

export class InternalSewobeUser extends SewobeUser {
    vorname: string;
    nachname: string;
    biername: string;
    mitgliedernummer: number;
    lastschrift: boolean;
    similarity: number;

}
