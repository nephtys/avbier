/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConfigService } from 'app/config.service';
import { SewobeService, MOCK_JSONSTRING, parseJsonObject, MOCK_PARSED } from './sewobe.service';
import { HttpModule, Http, ConnectionBackend, RequestOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import 'rxjs/Rx';
import { FormsModule } from '@angular/forms';


describe('SewobeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, FormsModule],
      providers: [
        ConfigService,
        HttpModule,
         {provide: XHRBackend, useClass: MockBackend},
          SewobeService        ]
    });
  });

  it('should ...', inject([SewobeService], (service: SewobeService) => {
    expect(service).toBeTruthy();
  }));


  it('should create right date', inject([SewobeService], (service: SewobeService) => {
    const dateOfBill: Date = new Date('2017-06-24T12:30:00Z');
    expect(dateOfBill.getTime()).toBe(1498307400000);
    expect(dateOfBill.getMonth()).toBe(6 - 1);
    expect(dateOfBill.getDate()).toBe(24);
  }));



it('should parse json correctly',  inject([SewobeService], (service: SewobeService) => {
    const json = JSON.parse(MOCK_JSONSTRING);
    const arr = parseJsonObject(json);
    expect(arr.length === 3).toBeTruthy();
    expect(arr[1].mitgliedernummer  === 11294).toBeTruthy();
    expect(arr[2].biername  === 'Gruin').toBeTruthy();
    expect(arr[0].vorname  === 'Frederik').toBeTruthy();
    expect(arr[0].lastschrift).toBeTruthy();
    expect(!arr[1].lastschrift).toBeTruthy();



    expect(MOCK_PARSED.length === 3).toBeTruthy();
    expect(MOCK_PARSED[1].mitgliedernummer  === 11294).toBeTruthy();
    expect(MOCK_PARSED[2].biername  === 'Gruin').toBeTruthy();
    expect(MOCK_PARSED[0].vorname  === 'Frederik').toBeTruthy();
    expect(MOCK_PARSED[0].lastschrift).toBeTruthy();

}));

});
