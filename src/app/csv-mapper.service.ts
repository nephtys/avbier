import { Injectable } from '@angular/core';
import { User } from './user';
import { Drink } from './drink';
import { MappingService } from 'app/mapping.service';


interface InputParser {
  /**
   * mapInputCSV
input: String   */
  parseInputCSVtoUsers(inputCSV: String, csvDelimiter: string): CSVResult;
}

export interface CSVResult {
  inputUsers: User[];
  drinks: Drink[];
  remarkWith20Characters: string;
  date: Date;
}

@Injectable()
export class CsvMapperService implements InputParser {


  constructor(private mappingService: MappingService) {
  }
  public parseInputCSVtoUsers(inputCSV: string, csvDelimiter: string): CSVResult {

    const result = {
      inputUsers: [],
      drinks: [],
      date: new Date(),
      remarkWith20Characters: ''
    } as CSVResult;

    const allRows = inputCSV.split('\n');
    if (allRows.length < 2) {
      console.log('Less than 2 rows');
      throw new Error('Less than 2 rows.');
      // return result; //prevent out of bounds exception if using shift()
    }
    const drinkNamesLine = allRows.shift();
    const drinkPricesLine = allRows.shift();

    const csvDrinks = drinkNamesLine.split(csvDelimiter).map(s => s.trim());
    const prices = drinkPricesLine.split(csvDelimiter).map(s => s.trim());

    if (csvDrinks.length !== prices.length) {
      throw new Error('unequal drink name / prices lines');
      /*return {
        inputUsers : [],
        drinks : []
      } as CSVResult;*/
    }

    const remarkLabel = csvDrinks[0].trim();
    const dateString = prices[0];
    console.log('reading date from : ' + dateString.toString());
    let date;

    date = this.parseDate(dateString);

    // if (remarkLabel !== 'Datum') {
      result.remarkWith20Characters = remarkLabel.substring(0, 20);
    /*} else {
      result.remarkWith20Characters = 'KantinenAutomapper';
    }*/

    if (isNaN(date)) {
      // do something to avoid error date
      throw new Error('Date is NaN.');
    } else {
      console.log('setting date to : ' + date.toString());
      result.date = date;
    }

    csvDrinks.splice(0, 2);
    prices.splice(0, 2);

    this.parseCSVDrinks(csvDrinks, prices, result);


    this.parseUserAndDrinksHorizontally(allRows, csvDelimiter, result);

    console.log('Normal return while parsing ' + inputCSV);
    return result;
  }

  public parseDate(date: string): Date {
    // string toShortDates = date.replace('-', '/');
    const toShortDates = date.split('-');
    // var newDate = new Date(toShortDates);
    const newDate = new Date(parseInt(toShortDates[2], 10), parseInt(toShortDates[1], 10) - 1, parseInt(toShortDates[0], 10));
    return newDate;
  }

  private parseCSVDrinks(csvDrinks: string[], prices: string[], resultArrays: CSVResult) {
    if (csvDrinks.length !== prices.length) {
      return false;
    }

    for (let i = 0; i < csvDrinks.length; i++) {
      const newDrink = new Drink(csvDrinks[i].trim(), parseFloat(prices[i].replace(',', '.')));
      resultArrays.drinks.push(newDrink);
    }
  }

  private parseUserAndDrinksHorizontally(allRows: string[], csvDelimiter: string, resultArrays: CSVResult) {
    const numOfDrinks = resultArrays.drinks.length;

    for (const userIterator of allRows) {


      const splitUserRow: string[] = userIterator.split(csvDelimiter);
      if (splitUserRow[0].trim() === 'Biername') {
        continue;
      }

      const csvUser = new User(splitUserRow[0], splitUserRow[1]);
      const firsttwocells: string[] = splitUserRow.splice(0, 2);
      if (splitUserRow.length !== numOfDrinks) {
        console.log('SKIPPING WRONG NUMBER LINE');
        continue;
      }

      console.log('Parsing User:');

      for (let i = 0; i < splitUserRow.length; i++) {
        if (!isNaN(parseInt(splitUserRow[i], 10))) {
          csvUser.addDrinkWithCount(resultArrays.drinks[i], parseInt(splitUserRow[i], 10));
          console.log(csvUser.biername + ', drink : ' + resultArrays.drinks[i].name + ' = ' + parseInt(splitUserRow[i], 10));
        }
      }
      if (firsttwocells[1].trim().length === 0) {
        // what to do when NOT or remaining columns doesnt fit drinks
        resultArrays.inputUsers.push(csvUser);
      }
    }
  }
}
