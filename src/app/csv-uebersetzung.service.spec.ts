/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CsvUebersetzungService } from './csv-uebersetzung.service';
import 'rxjs/Rx';
import { FormsModule } from '@angular/forms';

describe('CsvUebersetzungService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
  imports: [ FormsModule ],
      providers: [CsvUebersetzungService]
    });
  });

  it('should ...', inject([CsvUebersetzungService], (service: CsvUebersetzungService) => {
    expect(service).toBeTruthy();
  }));
});
