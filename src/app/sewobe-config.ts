export interface SewobeConfig {
    sewobeUrl: string;
    sewobePasswort: string;
    sewobeAbfrageNr: number;
    sewobeUsername: string;
}
