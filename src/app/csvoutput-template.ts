export class CSVOutputTemplate {

    private readonly raw_header = 'Mitgliedsnummer;R vs G;Rechnungsnr.;Rechnungsname;Rechnungsdatum;Positionsnr.;' +
    'Positionsname;Positionsbeschreibung;Anzahl;Preis pro Einheit;2 == Lastschrift und 1 == Ueberweisung;' +
    'Empfang per Mail;Zahlungsziel in Tagen;SEPA Intervall (0 fuer einmalig);Datum Rechnungsstellung;Datum ' +
    'Faelligkeit;Datum Positionsende;Mehrwertsteuersatz;Beschreibung; Spendenfähig;' +
     'Spende;Buchhaltungskonto;Steuerschluessel;Unterkonto Kantine';

    readonly mailEmpfang = 2;
    readonly mehrWertSteuer = 0;
    readonly buchHaltungsKonto = 1112;
    readonly steuerSchluessel = 1;
    readonly unterKontoKantine = 8293;
    readonly zahlungsziel = 30;
    readonly rVG = 2;
    readonly spendenfaehig = 0;
    readonly spende = ''
    beschreibung = 'auto-gen by avbier'

    getHeader(): string[] {
        return this.raw_header.split(';');
    }
}
