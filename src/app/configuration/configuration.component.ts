
import { Component, OnInit } from '@angular/core';
import { SewobeConfig } from 'app/sewobe-config';
import { ConfigService } from 'app/config.service';
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  config: SewobeConfig;
  json = '';

  constructor(private configurationService: ConfigService) {
      configurationService.getConfig().subscribe(t => {
          this.config = JSON.parse(JSON.stringify(t));
          this.json = JSON.stringify(t);
      });
  }

  ngOnInit() {
  }

  keyEntered(): void {
    this.json = JSON.stringify(this.config);
    this.config = JSON.parse(this.json);
  }

  reload(): void {
    this.configurationService.reemit();
  }

  store(): void {
    this.configurationService.store(this.config);
    console.log('Storing to localStorage:');
    console.log(this.config);
  }

  focusoutJson(): void {
    try {
    this.config = JSON.parse(this.json);
    } catch (e) {
      alert('Invalid Json entered: ' + e);
      this.keyEntered();
    }
  }

}
