/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ConfigService } from 'app/config.service';
import { ConfigurationComponent } from 'app/configuration/configuration.component';
import { AppComponent } from 'app/app.component';
import { FormsModule } from '@angular/forms';
import { SelectionListelementComponent } from 'app/selection-listelement/selection-listelement.component';

describe('ConfigurationComponent', () => {
  let component: ConfigurationComponent;
  let fixture: ComponentFixture<ConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
  imports: [ FormsModule ],
      providers: [ConfigService],
      declarations: [
        AppComponent,
        SelectionListelementComponent,
        ConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
