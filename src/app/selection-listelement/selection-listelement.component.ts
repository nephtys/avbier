import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MappingService } from 'app/mapping.service';
import { SewobeUser } from 'app/sewobe-user';
import { SewobeService } from 'app/sewobe.service';
import { ConfigService } from 'app/config.service';
    declare var require: any;
// import { Stringsimilarity} from 'string-similarity';


export function similarity(str1, str2): number {
      const leven = require('leven');
      const n = Math.max(str1.length, str2.length);
      return 1.0 - (leven(str1, str2) / n);
}

@Component({
  selector: 'app-selection-listelement',
  templateUrl: './selection-listelement.component.html',
  styleUrls: ['./selection-listelement.component.css']
})
export class SelectionListelementComponent implements OnChanges {

  // tslint:disable-next-line:no-inferrable-types
  @Input() username: string = 'N/A';



  candidates: SewobeUser[];
  selectedValue: SewobeUser = null;

  readonly normalcolor = 'white';
  readonly highlightcolor = 'red';
  readonly infocolor = 'blue';

  readonly warning_similarity_threshold = 0.67;
  backgroundcolor: string = this.normalcolor;

  sortInPlaceByComparison(users: SewobeUser[], thisusername: string): SewobeUser[]  {

    console.log('Trying to sort users: ')
    console.log(users);
    console.log('Against: ' + thisusername);

      users.sort((u1, u2) => {

        if (u1.similarity < 0) {
            if (u1.biername.trim().length > 0) {
              u1.similarity = similarity(u1.biername, thisusername);
            } else {
              u1.similarity = similarity(u1.vorname, thisusername);
            }
        }
        if (u2.similarity < 0) {
            if (u2.biername.trim().length > 0) {
              u2.similarity = similarity(u2.biername, thisusername);
            } else {
              u2.similarity = similarity(u2.vorname, thisusername);
            }
        }


        if (u1.similarity < u2.similarity) {
          return 1;
        }
        if (u2.similarity < u1.similarity) {
          return -1;
        }
        return 0;
      });

      console.log(users);
      return users;
  }

  selectCandidate(candidateNr: number): void {
    if (this.candidates != null) {
    for (const c of this.candidates) {
      if (c.mitgliedernummer === candidateNr) {
        this.selectedValue = c;
      }
    }
  }
}



  onSelect(candidateEvent: SewobeUser): void {
    return this.selectedCandidate(candidateEvent);
  }

  selectedCandidate(candidate: SewobeUser): void {
    this.mappingService.addMapping(this.username, candidate.mitgliedernummer);
    this.calcColor();
  }

  constructor(
    private mappingService: MappingService,
    private sewobeService: SewobeService
   ) {
    // listen for config changes, and call sewobe with the config afterwards. Lastly, sort result and set to local variable
    this.sewobeService.usersFromSewobe.subscribe(users => {
      this.candidates = JSON.parse(JSON.stringify(users));
    });
   }


   calcColor() {
     if (this.selectedValue.similarity < this.warning_similarity_threshold) {
     // if below threshold
      this.backgroundcolor = this.highlightcolor;
     } else {
     // else
        if (this.candidates.length >= 2 && this.candidates[0].similarity === this.candidates[1].similarity) {
        // if two identical top values
          this.backgroundcolor = this.infocolor;
        } else {
        // else normal color
      this.backgroundcolor = this.normalcolor;
        }
     }
   }


  ngOnChanges(changes: SimpleChanges): void {

    this.sortInPlaceByComparison(this.candidates, this.username);


    // select old mapping on Input change
    const m = this.mappingService.getMappings();
    // console.log(m);
    if (m.has(this.username)) {
      // console.log('Found username in mapping');
      this.selectCandidate(m.get(this.username));
    } else if (this.candidates != null && this.candidates.length > 0) {
      // console.log('Using default');
      this.selectCandidate(this.candidates[0].mitgliedernummer);
    } else {
      // console.log('Called else');
    }
    this.calcColor();
    if (this.selectedValue !== null) {
      this.mappingService.addMapping(this.username, this.selectedValue.mitgliedernummer);
    }
  }

}
