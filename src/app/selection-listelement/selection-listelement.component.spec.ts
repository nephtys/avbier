/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SelectionListelementComponent } from './selection-listelement.component';
import { ConfigurationComponent } from 'app/configuration/configuration.component';
import { AppComponent } from 'app/app.component';
import { HttpModule, XHRBackend } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { MockBackend } from '@angular/http/testing';
import { ConfigService } from 'app/config.service';
import { SewobeService } from 'app/sewobe.service';
import { MappingService } from 'app/mapping.service';

describe('SelectionListelementComponent', () => {
  let component: SelectionListelementComponent;
  let fixture: ComponentFixture<SelectionListelementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ConfigurationComponent,
        SelectionListelementComponent
      ],
      imports: [HttpModule, FormsModule],
      providers: [
        HttpModule,
         {provide: XHRBackend, useClass: MockBackend},
         ConfigService,
          SewobeService, MappingService,         ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionListelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
