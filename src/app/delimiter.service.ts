import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DelimiterService {
  public OUTPUT_LOCAL_STORAGE_KEY = 'AVBIER_CSV_OUTPUT_DELIMITER';
  public INPUT_LOCAL_STORAGE_KEY = 'AVBIER_CSV_INPUT_DELIMITER';
  public DECIMAL_LOCAL_STORAGE_KEY = 'AVBIER_CSV_DECIMAL_DELIMITER';

  private outputDelimiterSubject: BehaviorSubject<string>;
  private inputDelimiterSubject: BehaviorSubject<string>;
  private decimalDelimiterSubject: BehaviorSubject<string>;

  constructor() {
    const outd: string = localStorage.getItem(this.OUTPUT_LOCAL_STORAGE_KEY);
      if (outd && outd.length > 0) {
        this.outputDelimiterSubject = new BehaviorSubject(outd);
      } else {
        this.outputDelimiterSubject = new BehaviorSubject(';');
      }

      const ind: string = localStorage.getItem(this.INPUT_LOCAL_STORAGE_KEY);
      if (ind && ind.length > 0) {
        this.inputDelimiterSubject = new BehaviorSubject(ind);
      } else {
        this.inputDelimiterSubject = new BehaviorSubject(';');
      }

      const dec: string = localStorage.getItem(this.DECIMAL_LOCAL_STORAGE_KEY);
      if (dec && dec.length > 0) {
        this.decimalDelimiterSubject = new BehaviorSubject(dec);
      } else {
        this.decimalDelimiterSubject = new BehaviorSubject(',');
      }

   }


  outputDelimiter(): Observable<string> {
    return this.outputDelimiterSubject;
  }
  emitOutputDelimiter(delimiter: string): void {
    console.log('Setting delimiter to: ' + delimiter);
    localStorage.setItem(this.OUTPUT_LOCAL_STORAGE_KEY, delimiter);
    this.outputDelimiterSubject.next(delimiter);
  }

  emitInputDelimiter(delimiter: string): void {
    console.log('Setting delimiter to: ' + delimiter);
    localStorage.setItem(this.INPUT_LOCAL_STORAGE_KEY, delimiter);
    this.inputDelimiterSubject.next(delimiter);
  }
  inputDelimiter(): Observable<string> {
    return this.inputDelimiterSubject;
  }


  emitDecimalDelimiter(delimiter: string): void {
    console.log('Setting decimal delimiter to: ' + delimiter);
    localStorage.setItem(this.DECIMAL_LOCAL_STORAGE_KEY, delimiter);
    this.decimalDelimiterSubject.next(delimiter);
  }
  decimalDelimiter(): Observable<string> {
    return this.decimalDelimiterSubject;
  }

}
