import { User } from 'app/user';
import { SewobeUser } from 'app/sewobe-user';

export class CSVOutputUser {
    constructor(
    public beerlistUser: User,
    public sewobeUser: SewobeUser) {

    }
}
