/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MappingService } from 'app/mapping.service';
import { CsvMapperService, CSVResult } from './csv-mapper.service';


// tslint:disable-next-line:max-line-length
const TESTINPUT = `CoolRemark With a very long message and some weird sentence construction;Posten; Hefe;Apfelschorle;Augustiner Helles;Club Mate;Coca Cola;Coca Cola Zero;Cola 1,5l;Cola Mix;Edel;Energy Drink 1,5l;Fanta;Grape;Kräusen;Mezzo Mix;Pils;Pils(Alkoholfreies);Rothaus;Schneider Weissbräu;Schwarz-Gold;Wasser 0,5;Wasser 0,7;Wegbier (Pils);Weizen (Alkoholfrei);Zitronen Limo
01-03-2017;Sonderspalte;0,95;0,85;0,85;1,0;0,95;0,95;0,80;0,85;0,95;1,0;0,95;0,75;0,95;0,95;0,95;1,0;0,85;0,85;0,95;0,10;0,40;1,5;1,0;0,85
Biername;;;;;;;;;;;;;;;;;;;;;;;;
-durch-;;;1;;3;;;;;;;;;;;;;;;;;;;;
AlpinA;;;;;;1;;;;;;;;;1;;;;;;3;1;;;
Baller;;;;;;;;;;2;;;;10;3;41;;;;;;1;2;;
Binär;;5;7;;13;;1;;1;;;;;16;1;7;;;;;;2;;5;1
Blytz;;;;;;1;;;;;;;4;;;1;;;;;;1;;;
Bro;;;2;;;1;;;;;;;1;1;;7;;;;;;3;;;1
Chøí;;1;;6;1;;;;;;;;1;;;6;;4;;;;;2;;
Com;;;;;;;;;;2;;;;;;2;;;;;;;;;
Couleur;NOT;3;1;;;;;;;;;;;18;;32;;;1;;;;2;;
Feuer;;;;;;;;;;;;;;;1;;;;;;;;;;
FinGer;;5;13;;1;8;;;4;;;2;1;14;;2;;;;;;3;;;2
Gruin;;1;6;;;16;5;;;;;;;10;1;6;1;;;;;3;;;
Husch;;;;;;;;;;;;;;;;;;;;;;1;;;
IchH;;;6;;;;;;;3;;;;7;;23;;;;;;2;;;
Ilona;;;;;;1;;;;;;;1;;1;;;;;;;;;;
JacKie;;;;;4;2;1;;;;;;;;1;2;;;;;;;;;
Jaegk;;;;;;;;;;;;;;;1;14;;;;;;;;;
Kantine;NOT;;;;;;;;;;;;;;;;;;;;;1;;;
LEGo;;;;;6;;;;;;;;1;;;2;;;;;;;;;
LiOn;;;1;;;;;1;;;;;;1;;4;;;;;;;;;
Lin-Gu;;;;;;8;;;;;;;3;1;1;;;;;;;1;;;
Luca;;1;15;;27;;2;;6;;;3;5;9;2;7;;;;2;;;;;11
Nixz;;;;;;;;;;;;;;;;1;;;;;6;;;;
Owi;;;;;1;;;;;;;;;9;;7;;;;;;;;;
Patrik;;1;22;;15;16;;;1;;;;3;2;1;5;;;;;1;3;;;4
Rise;;;;;;1;;;;;;;;;;;;;;;;2;;;
Rose;;;;;;1;;;;;;1;1;;;;;;;;;;;;
ScHnitte;;;1;;;1;;;;;;;;;;;;;;;;;;;
Seuv;;;;;;;1;;;;;;;;;;;;;;;;;;
Shiva;;;;;;;;;;;;;;2;;17;1;;;;;;;;1
SiSi;;;;;;1;;;;;;;;;;;;;;;;;;;
SoulJAH;;;;;;1;;;;;;;;;;;;;;;;;;;
Tiden;;;;;;;;;;;;;;4;3;2;;;;;;1;;;2
Vall;;;13;;9;5;;;;;;;;11;;4;;;;;;1;;;2
Veilchen;;;;;;;1;;;;;;;;;;;;;;;;;;
Würfel;;;;;;;;;1;;;;;;;3;;;;;;;;;
dJ.;;;1;;3;2;;;;;;;;;;8;;;;;;;;;
fischers;;;;;1;;;;;;;;1;1;;;;;;;;1;;;
licht®;;;;;10;;;;;;;;;14;;2;;;;;;6;;;1
länka;;;;;;1;;;;;;8;24;2;;276;;;;;;;;;
mitch;;;2;;;;;;1;;;;;1;;;;;;;;;;;
pr!ma;;;;;;;2;;;;;;1;;;;;;;;;1;;;
rad(i);;;1;;4;1;;;;;;;;3;;11;;;;;;;;;
sWiNg;;;;;;;;;;3;;;;4;;15;;;;;;;;;
style;;;1;;;9;1;;11;;1;;2;;;3;;;;;;;;;2
tAff;;;;;;;;;;;;;4;;;;;;;;;;;;1
tChörch;;;3;;;3;;;;;;;;4;1;2;;;;;;;;;`;



function get(result: CSVResult, biername: string, drinkname: string): number {
  let r = 0;
  for (const u of result.inputUsers) {
    for (const d of u.drinkCountTouples) {
      if (u.biername === biername && drinkname === d.drink.name) {
        r = d.count;
      }
    }
  }
  return r;
}

describe('CsvMapperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MappingService, CsvMapperService]
    });
  });

  it('should parse dates correctly', inject([CsvMapperService], (service: CsvMapperService) => {
    expect(service.parseDate('01-03-2017').getDate()).toBe(1);
    expect(service.parseDate('01-03-2017').getMonth() + 1).toBe(3);
    expect(service.parseDate('01-03-2017').getFullYear()).toBe(2017);
  }));


  it('should parse other dates correctly', inject([CsvMapperService], (service: CsvMapperService) => {
    expect(service.parseDate('12-07-2017').getDate()).toBe(12);
    expect(service.parseDate('12-07-2017').getMonth() + 1).toBe(7);
    expect(service.parseDate('12-07-2017').getFullYear()).toBe(2017);
  }));



  it('should parse complex files', inject([CsvMapperService], (service: CsvMapperService) => {
    const l = service.parseInputCSVtoUsers(TESTINPUT, ';');
    expect(l.drinks.length).toBe(24);
    expect(l.inputUsers.length).toBe(45); // 47 raw users with 2 with NOT metadata == 45
    expect(l.date.getDate()).toBe(1);
    expect(l.date.getMonth() + 1).toBe(3);
    expect(l.date.getFullYear()).toBe(2017);

    // console.log(JSON.stringify(l));

    // console.log(JSON.stringify(l.inputUsers[6]));

    expect(get(l, '-durch-', 'Club Mate')).toBe(3);
    expect(get(l, '-durch-', 'Apfelschorle')).toBe(1);
    expect(get(l, '-durch-', 'Weizen (Alkoholfrei)')).toBe(0);
    expect(get(l, 'Binär', 'Weizen (Alkoholfrei)')).toBe(5);
    expect(get(l, 'Chøí', 'Hefe')).toBe(1);
    expect(get(l, 'Chøí', 'Augustiner Helles')).toBe(6);
    expect(get(l, 'Gruin', 'Hefe')).toBe(1);
    expect(get(l, 'Gruin', 'Apfelschorle')).toBe(6);
    expect(get(l, 'Gruin', 'Wasser 0,7')).toBe(3);
    expect(get(l, 'tChörch', 'Kräusen')).toBe(4);
    expect(get(l, 'tAff', 'Zitronen Limo')).toBe(1);

    expect(l.remarkWith20Characters).toBe('CoolRemark With a ve');
  }));

});
