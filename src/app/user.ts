
import {Drink} from './drink';
import {DrinkNumTouple} from './drink-num-touple';


export class User {
    biername: string;
    additionalLine: string;
    drinkCountTouples: DrinkNumTouple[];

    static manualBuild(biername: string, not: boolean, drinks: Drink[], counts: number[]): User {
        const user = new User(biername, (not ? 'NOT,' : ',') + counts.join(','));
        for (let i = 0; i < drinks.length; i++) {
            user.addDrinkWithCount(drinks[i], counts[i]);
        }
        return user;
    }

    constructor(biername: string, additionalLine: string) {
        this.biername = biername;
        this.additionalLine = additionalLine;
        this.drinkCountTouples = []
    }


    public addDrinkWithCount(drink: Drink, count: number) {

        if (drink != null && count != null && !isNaN(count) && count > 0) {

        this.drinkCountTouples.forEach(element => {
            if (element.drink.name === drink.name) {
                element.count += count;
                return true;
            }
        });
        this.drinkCountTouples.push(new DrinkNumTouple(drink, count));

        return true;
        } else {
        return false;
    }
    }

    public addDrink(drink: Drink) {
        this.addDrinkWithCount(drink, 1);
    }
}

