import { CSVOutputUser } from 'app/csvoutput-user';
import { CSVOutputTemplate } from 'app/csvoutput-template';

export interface CSVOutputBuilder {

    /**
     * takes the users that were mapped to sewobe
     * and a date and a template
     * and builds the CSV cells needed for the output file
     *
     * it also starts with the csv header line (found in template)
     *
     * missing afterwards is only the delimiter between cells
     */
    buildOutput(mappedUsers: CSVOutputUser[], dateOfBill: Date, template: CSVOutputTemplate, decimalCharacter: string): string[][];

}
