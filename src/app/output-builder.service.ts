import { Injectable } from '@angular/core';
import { CSVOutputBuilder } from 'app/csvoutput-builder';
import { CSVOutputUser } from 'app/csvoutput-user';
import { CSVOutputTemplate } from 'app/csvoutput-template';
import { SewobeUser } from 'app/sewobe-user';
import { User } from 'app/user';
import { Drink } from 'app/drink';
import { MOCK_JSONSTRING, MOCK_PARSED } from 'app/sewobe.service';

@Injectable()
export class OutputBuilderService implements CSVOutputBuilder {
  static readonly TEST_INPUT: CSVOutputUser[] = [
    new CSVOutputUser(User.manualBuild('', false, [new Drink('Edel', 0.95)], [4]),
      (MOCK_PARSED)[0]),
    new CSVOutputUser(User.manualBuild('', false, [    new Drink('Edel', 0.95), new Drink('Kräussen', 0.95),
, new Drink('Nothing', 0)], [4, 2, 0]), (MOCK_PARSED)[2]),
  ];
  static readonly TEST_OUTPUT: string[][] = [
    new CSVOutputTemplate().getHeader(),
    ['11293', '2', '17062411293', 'Kantinenrechnung 06/2017', '24.06.2017',
      '1', 'Edel', 'Edel', '4', '0,95', '2', '2', '30', '0', '24.06.2017',
      '01.07.2017', '31.05.2117', '0', 'auto-gen by avbier', '0', '', '1112', '1', '8293'],
    ['10831', '2', '17062410831', 'Kantinenrechnung 06/2017', '24.06.2017',
      '1', 'Edel', 'Edel', '4', '0,95', '1', '2', '30', '0', '24.06.2017',
      '01.07.2017', '31.05.2117', '0', 'auto-gen by avbier', '0', '', '1112', '1', '8293'],
    ['10831', '2', '17062410831', 'Kantinenrechnung 06/2017', '24.06.2017',
      '2', 'Kräussen', 'Kräussen', '2', '0,95', '1', '2', '30', '0', '24.06.2017',
      '01.07.2017', '31.05.2117', '0', 'auto-gen by avbier', '0', '', '1112', '1', '8293'],
  ];

  private padWithZeroes(x: number, digits: number): string {
    let suffix = x.toString();
    while (suffix.length < digits) {
      suffix = '0' + suffix;
    }
    return suffix;
  }

  private dateToString(date: Date): string {
    return this.padWithZeroes(date.getDate(), 2) + '.' +
      this.padWithZeroes(date.getMonth() + 1, 2) + '.' + this.padWithZeroes(date.getFullYear(), 4);
  }

  private dateToCompressedString(date: Date): string {
    return this.padWithZeroes(date.getFullYear() % 100, 2) + this.padWithZeroes(date.getMonth() + 1, 2) +
      this.padWithZeroes(date.getDate(), 2);
  }

  buildOutput(mappedUsers: CSVOutputUser[],
    dateOfBill: Date, template: CSVOutputTemplate, decimalCharacter: string): string[][] {
    const d = dateOfBill.getTime();

    const output: string[][] = [template.getHeader()];
    const billDate: string = this.dateToString(dateOfBill);
    const rvG: string = template.rVG.toString();
    const billName: string = 'Kantinenrechnung ' + this.padWithZeroes(dateOfBill.getMonth() + 1, 2) + '/' + dateOfBill.getFullYear();

    const positionEnde: string = this.dateToString(new Date(d + (1000 * 60 * 60 * 24 * 365 * 100)));
    const faelligkeit: string = this.dateToString(new Date(d + (1000 * 60 * 60 * 24 * 7)));


    const receiveViaEmail: string = template.mailEmpfang.toString();
    const zahlungsziel: string = template.zahlungsziel.toString();
    const sepaIntervall = '0';
    const rechungsstellung: string = this.dateToString(dateOfBill);
    const taxRate: string = template.mehrWertSteuer.toString();
    const subTaxNr: string = template.buchHaltungsKonto.toString();
    const taxKey: string = template.steuerSchluessel.toString();
    const kantinenSubKonto: string = template.unterKontoKantine.toString();

    for (const user of mappedUsers) {
      // console.log('analyzing:');
      // console.log(user);

      const userNr: string = user.sewobeUser.mitgliedernummer.toString();
      const lastschrift: string = (user.sewobeUser.lastschrift ? 2 : 1).toString();

      for (let i = 0; i < user.beerlistUser.drinkCountTouples.length; i++) {
        const item = user.beerlistUser.drinkCountTouples[i];

        const amount: string = item.count.toString();
        console.log( user.beerlistUser.biername + ' ' + item.drink.name + ' = ' + amount);

        const pricePerUnit: string = item.calculateAveragePriceString(decimalCharacter);
        const positionNr: string = (i + 1).toString();
        const positionName: string = item.drink.name;
        const positionDescription: string = item.drink.name;
        const billNumber: string = this.dateToCompressedString(dateOfBill) + userNr;// + this.padWithZeroes((i + 1), 2);
        const description = template.beschreibung;
        const donationkey: string = template.spendenfaehig.toString();
        const donationdetail = template.spende;


        if (item.count > 0) {
          output.push([
            userNr, rvG, billNumber, billName, billDate,
            positionNr, positionName, positionDescription, amount,
            pricePerUnit, lastschrift, receiveViaEmail, zahlungsziel,
            sepaIntervall, rechungsstellung, faelligkeit, positionEnde,
            taxRate, description, donationkey, donationdetail, subTaxNr, taxKey, kantinenSubKonto
          ]);
          // console.log(output[output.length - 1]);
        }
        // console.log(output.map(s => s.join(',')).join('\n'));
      }
    }

    return output;
  }


  /*
'Mitgliedsnummer;R vs G;Rechnungsnr.;Rechnungsname;Rechnungsdatum;Positionsnr.;' +
    'Positionsname;Positionsbeschreibung;Anzahl;Preis pro
    Einheit;2 == Lastschrift und 1 == Ueberweisung;' +
    'Empfang per Mail;Zahlungsziel in Tagen;SEPA Intervall
    (0 fuer einmalig);Datum Rechnungsstellung;Datum ' +
    'Faelligkeit;Datum Positionsende;Mehrwertsteuersatz;Buchhaltungskonto;
    Steuerschluessel;Unterkonto Kantine';

  */

  constructor() { }

}
