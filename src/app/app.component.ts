import { Component } from '@angular/core';
import { SewobeService, MOCK_JSONSTRING } from 'app/sewobe.service';
import { XHRBackend, HttpModule, ConnectionBackend, Response, ResponseOptions, BaseRequestOptions, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { SewobeConfig } from 'app/sewobe-config';
import { SewobeUser } from 'app/sewobe-user';
import { ConfigService } from 'app/config.service';
import { MappingService } from 'app/mapping.service';
import { CsvUebersetzungService } from 'app/csv-uebersetzung.service';
import { CsvMapperService, CSVResult } from 'app/csv-mapper.service';
import { CSVOutputTemplate } from 'app/csvoutput-template';
import { DelimiterService } from 'app/delimiter.service';
import { Observable } from 'rxjs/Observable';
import { Output } from 'app/output';
import { CSVOutputUser } from 'app/csvoutput-user';
import { OutputBuilderService } from 'app/output-builder.service';
import { Base64 } from 'js-base64';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    /*MockBackend,
    BaseRequestOptions,
    {
      provide: Http,
      deps: [MockBackend, BaseRequestOptions],
      useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        backend.connections.subscribe((connection: any) => {
          connection.mockRespond(new Response(new ResponseOptions({
            body: MOCK_JSONSTRING
          })));
        });
        return new Http(backend, options);
      }
    },*/
    ConfigService,
    SewobeService,
    MappingService,
    CsvUebersetzungService,
    CsvMapperService,
    DelimiterService,
    OutputBuilderService

  ]
})
export class AppComponent {
  title = 'Avbier';
  sewobeUsers: SewobeUser[] = [];
  bierlisteUsers: CSVResult = {
    inputUsers: [],
    drinks: []
  } as CSVResult;

  csvDelimiter: string;

  csvOutputDelimiter: string;

  decimalDelimiter: string;


  constructor(private sewobeService: SewobeService,
    private csvUebersetzungService: CsvUebersetzungService,
    private csvMapperService: CsvMapperService,
    private mappingService: MappingService,
    private delimiterService: DelimiterService,
    private outputBuilderService: OutputBuilderService
  ) {
    sewobeService.usersFromSewobe.subscribe(t => {
      this.sewobeUsers = t;
    });
    this.delimiterService.inputDelimiter().subscribe(d => this.csvDelimiter = d);
    this.delimiterService.outputDelimiter().subscribe(d => this.csvOutputDelimiter = d);
    this.delimiterService.decimalDelimiter().subscribe(d => this.decimalDelimiter = d);

    // given csv and delimiter, use csvMapperService to split up csv and write to arrays
    const inputParsedRx: Observable<CSVResult> = this.csvUebersetzungService.inputCSV()
      .flatMap(csv => this.delimiterService.inputDelimiter().map(delimiter => {
        console.log('parsing CSV');
        return this.csvMapperService.parseInputCSVtoUsers(csv, delimiter);
      }));
    inputParsedRx.subscribe(t => {
      this.bierlisteUsers = t;
      console.log('setting parsed csv:');
      console.log(this.bierlisteUsers);
    });




    let date: Date = new Date();

    this.mappingService.getMappingsRx().subscribe(e => console.log('Mapping Rx Triggered'));

    // bringing it all together for the output
    const outputFormattedRx: Observable<Output> = inputParsedRx.flatMap((inputCSV: CSVResult) => {
      return this.delimiterService.decimalDelimiter().flatMap((decimalDelimiter: string) => {
        console.log('Reaching inputParsedRx flatMap');
        return this.sewobeService.usersFromSewobe
          .flatMap((sewobeusers: SewobeUser[]) => {
            console.log('Reaching usersFromSewobe flatMap');
            return this.mappingService.getMappingsRx()
              .debounceTime(200).map((mapping: Map<string, number>) => {
                const mappedUsers: CSVOutputUser[] = [];
                inputCSV.inputUsers.forEach(
                  user => {
                    if (mappingService.hasMappedSewobeUser(user.biername)) {
                      mappedUsers.push(new CSVOutputUser(user, mappingService.getMappedSewobeUser(user.biername, sewobeusers)));
                    }
                  }
                );
                const template: CSVOutputTemplate = new CSVOutputTemplate();
                template.beschreibung = inputCSV.remarkWith20Characters;


                if (template.beschreibung === 'Datum') {
                  template.beschreibung = this.formatDateAsShortAbrechnung(inputCSV.date);
                }

                console.log('Found Mapped Users:');
                console.log(mappedUsers);
                date = inputCSV.date;
                return outputBuilderService.buildOutput(mappedUsers, date, template, decimalDelimiter);
              });
          });
      })
    })
      .flatMap((outputCSV: string[][]) => {
        console.log('Reaching outputCSV flatMap');
        return this.delimiterService.outputDelimiter().map(delimiter => {
          return new Output(date, delimiter, outputCSV);
        });
      });


    outputFormattedRx.subscribe(out => {
      console.log('output:');
      console.log(out);
      const csvfilename = out.date.getTime().toString() + '.csv';
      const csvfilecontent = out.cells.map(line => line.join(out.delimiter)).join('\n');
      this.setOutputCSVLink(csvfilename, csvfilecontent);
    });
  }

  private padWithZeroes(x: number, digits: number): string {
    let suffix = x.toString();
    while (suffix.length < digits) {
      suffix = '0' + suffix;
    }
    return suffix;
  }

  formatDateAsShortAbrechnung(date: Date): string {
    const c = "Abrechnung " + this.padWithZeroes(date.getDate(), 2) + '.' + this.padWithZeroes(date.getMonth() + 1, 2) + '.' + this.padWithZeroes(date.getFullYear() % 100, 2);
    if (c.length > 20) {
      console.log('ERROR WHILE CALLING formatDateAsShortAbrechnung(), output too long!');
    }
    return c;
  }

  public setOutputCSVLink(filename: string, filecontent: string) {
    console.log('trying to set output csv link');
    if (document.readyState === 'complete') {
      if (document.getElementById('csvOutputLinkAnchor')) {
        console.log('actually setting output csv link directly');
        document.getElementById('csvOutputLinkAnchor')
          .setAttribute('href', 'data:text/csv;base64,77u/' + Base64.encodeURI(filecontent));
        document.getElementById('csvOutputLinkAnchor')
          .removeAttribute('hidden');

        document.getElementById('csvOutputLinkAnchor')
          .setAttribute('download', filename);
      }
    } else {

      window.onload = () => {
        console.log('actually setting output csv link via onload event');
        document.getElementById('csvOutputLinkAnchor')
          .setAttribute('href', 'data:text/plain;base64,' + btoa(filecontent));

        document.getElementById('csvOutputLinkAnchor')
          .setAttribute('download', filename);
        document.getElementById('csvOutputLinkAnchor')
          .removeAttribute('hidden');
      };
    }
  }

  public fileChangeEvent(fileInput: any): void {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      const serv = this.csvUebersetzungService;
      reader.onload = function (e: any) {
        // a wild file content appears!
        console.log(reader.result);
        serv.emitInputCSVContent(reader.result);
      };
      // start file reading process and load content
      reader.readAsText(fileInput.target.files[0]);
    }
  }

  public inputDelimiterChangeEvent(delimiterInput: any): void {
    if (delimiterInput.target && delimiterInput.target.value.trim().length > 0) {
      this.delimiterService.emitInputDelimiter(delimiterInput.target.value.trim());
    }
  }

  public outputDelimiterChangeEvent(delimiterInput: any): void {
    if (delimiterInput.target && delimiterInput.target.value.trim().length > 0) {
      this.delimiterService.emitOutputDelimiter(delimiterInput.target.value.trim());
    }
  }

  public decimalDelimiterChangeEvent(delimiterInput: any): void {
    if (delimiterInput.target && delimiterInput.target.value.trim().length > 0) {
      this.delimiterService.emitDecimalDelimiter(delimiterInput.target.value.trim());
    }
  }

}
