import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class CsvUebersetzungService {

  private inputCSVSubject: ReplaySubject<string> = new ReplaySubject();

  constructor() {
   }



  emitInputCSVContent(csv: string): void {
    this.inputCSVSubject.next(csv);
  }



  inputCSV(): Observable<string> {
    return this.inputCSVSubject;
  }

}
