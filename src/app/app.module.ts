import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { SelectionListelementComponent } from './selection-listelement/selection-listelement.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfigurationComponent,
    SelectionListelementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
