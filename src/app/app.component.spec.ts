/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CsvMapperService } from 'app/csv-mapper.service';
import { MappingService } from 'app/mapping.service';
import { CsvUebersetzungService } from 'app/csv-uebersetzung.service';
import { ConfigService } from 'app/config.service';
import { BaseRequestOptions, Response, Http, ResponseOptions } from '@angular/http';
import { SewobeService, MOCK_JSONSTRING } from 'app/sewobe.service';
import { MockBackend } from '@angular/http/testing';
import { SelectionListelementComponent } from 'app/selection-listelement/selection-listelement.component';
import { ConfigurationComponent } from 'app/configuration/configuration.component';
import { AppComponent } from 'app/app.component';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
  imports: [ FormsModule ],
      providers: [
        MockBackend,
    BaseRequestOptions,
    {
      provide: Http,
      deps: [MockBackend, BaseRequestOptions],
      useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        backend.connections.subscribe((connection: any) => {
          connection.mockRespond(new Response(new ResponseOptions({
            body: MOCK_JSONSTRING
          })));
        });
        return new Http(backend, options);
      }
    },
      ConfigService, SewobeService,
        CsvMapperService, MappingService, CsvUebersetzungService],
      declarations: [
        AppComponent,
        ConfigurationComponent,
        SelectionListelementComponent
      ],
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Avbier'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Avbier');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Avbier');
  }));
});
