import { TestBed, inject } from '@angular/core/testing';

import { OutputBuilderService } from './output-builder.service';
import { CSVOutputTemplate } from 'app/csvoutput-template';
import { MOCK_PARSED } from 'app/sewobe.service';

describe('OutputBuilderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OutputBuilderService]
    });
  });

  it('should be created', inject([OutputBuilderService], (service: OutputBuilderService) => {
    expect(service).toBeTruthy();
  }));

  it('should result in right output', inject([OutputBuilderService], (service: OutputBuilderService) => {
    const dateOfBill: Date = new Date('2017-06-24T12:30:00Z');
    const result: string[][] = service.buildOutput(OutputBuilderService.TEST_INPUT, dateOfBill, new CSVOutputTemplate(), ',');
    const should: string[][] = OutputBuilderService.TEST_OUTPUT;
    expect(dateOfBill.getTime()).toBe(1498307400000);
    expect(dateOfBill.getMonth()).toBe(6 - 1 );
    expect(dateOfBill.getDate()).toBe(24);
    expect(result.length).toBe(should.length);
    for (let i = 0; i < result.length; i++) {
      expect(result[i].length).toBe(should[i].length)
      for (let j = 0; j < result[i].length; j++) {
        expect(result[i][j]).toBe(should[i][j]);
      }
    }
  }));
});
