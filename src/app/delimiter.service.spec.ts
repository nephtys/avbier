/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DelimiterService } from './delimiter.service';

describe('DelimiterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DelimiterService]
    });
  });

  it('should ...', inject([DelimiterService], (service: DelimiterService) => {
    expect(service).toBeTruthy();
  }));
});
