import {Drink} from './drink';

export class DrinkNumTouple {
    drink: Drink;
    count: number;


    constructor(drink: Drink, count: number) {
        this.drink = drink;
        this.count = count;
    }

   calculateTotal(): number {
       return Number((this.drink.price * this.count).toFixed(2));
   }

   calculateAveragePriceString(decimalcharacter: string): string {
       return this.drink.price.toFixed(2).replace('.', decimalcharacter);
   }
}
