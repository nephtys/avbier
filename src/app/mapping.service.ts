import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SewobeUser } from 'app/sewobe-user';

@Injectable()
export class MappingService {


  private readonly mappingObs: BehaviorSubject< Map<string, number>>;
  private readonly lastMapping: Map<string, number>;
  private readonly _exportString: Observable<string>;
  public readonly localStorageKey = 'SEWOBE_MAPPING_JSON3';
  private readonly sharedMappingRx: Observable<Map<string, number>>;

  constructor() {
    const str = localStorage.getItem(this.localStorageKey);
    // console.log("localStorage extracted: "+str);
    if (str != null && str.length > 0) {
      const mapObject = JSON.parse(str);
      // console.log(mapObject);
      if (mapObject) {
        this.lastMapping = new Map();

        for (const key in mapObject) {
  if (mapObject.hasOwnProperty(key)) {
    this.lastMapping.set(key, mapObject[key]);
  }
}
      } else {
        this.lastMapping = new Map();
      }
    }  else {

        this.lastMapping = new Map();
    }
    this.mappingObs = new BehaviorSubject(this.lastMapping);
    this._exportString = this.mappingObs.map(t => JSON.stringify(this.transform(t)));
    this.sharedMappingRx = this.mappingObs.asObservable().share();
  }


  private persist(mappingMap: Map<string, number>): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.transform(mappingMap)));
  }

  private transform(mappingMap: Map<string, number>): Object {
    const c = {};
    mappingMap.forEach((v, k, m) => {
      c[k] = v;
    });
    return c;
  }


  getMappedSewobeNrOrDefaultToUsername(bierlistenName: string, def: number): number {
    console.log('Searching ' + bierlistenName + ' in:' );
    console.log(this.getMappings());
    if (this.getMappings().has(bierlistenName)) {
      return this.getMappings().get(bierlistenName);
    } else {
      return def;
    }
  }

  hasMappedSewobeUser(bierlistenName: string): boolean {
    return this.getMappedSewobeNrOrDefaultToUsername(bierlistenName, -1) >= 0;
  }

  getMappedSewobeUser(bierlistenName: string, sewobeUsers: SewobeUser[]): SewobeUser {
    const nr: number = this.getMappedSewobeNrOrDefaultToUsername(bierlistenName, -1);
    if (nr < 0) {
      throw new Error('User not mapped: ' + bierlistenName);
    }
    for (const user of sewobeUsers) {
      if (user.mitgliedernummer === nr) {
        return user;
      }
    }
      throw new Error('User not mapped: ' + bierlistenName);
  }

  addMapping(bierlistenName: string, sewobeNr: number): void {
    console.log('adding Mapping ' + bierlistenName + ' - ' + sewobeNr);


    this.lastMapping.set(bierlistenName, sewobeNr);
    // persist to localStorage
    this.persist(this.lastMapping);

    console.log('New Mapping:');
    console.log(this.lastMapping);

    // emit over observable
    this.mappingObs.next(this.lastMapping);
  }


  getMappingsRx(): Observable<Map<string, number>> {
    return this.sharedMappingRx;
  }

  getMappings(): Map<string, number> {
    return this.lastMapping;
  }


  exportableString(): Observable<string> {
    return this._exportString;
  }

}
