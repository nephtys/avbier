import { Injectable, EventEmitter } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { SewobeConfig } from 'app/sewobe-config';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


export const MOCK_CONFIGURATION: SewobeConfig = {sewobeUrl : 'secret.json',
    sewobePasswort: 'password',
    sewobeAbfrageNr: 80,
    sewobeUsername: 'username'};

@Injectable()
export class ConfigService {
  private readonly config: BehaviorSubject<SewobeConfig>;
  private lastConfig: SewobeConfig;
  public readonly localStorageKey = 'SEWOBE_CONFIG_JSON';



  public parseConfigOrNull(jsonStr: string): SewobeConfig {
     try {
    const json = JSON.parse(jsonStr);
    let url = '';
    let abfrage = 0;
    let password = '';
    let username = '';
    if (json != null) {
      if (json.sewobeUrl != null) {
        url = json.sewobeUrl;
      }
      if (json.sewobePasswort != null) {
        password = json.sewobePasswort;
      }
      if (json.sewobeUsername != null) {
        username = json.sewobeUsername;
      }
      if (json.sewobeAbfrageNr != null) {
        abfrage = json.sewobeAbfrageNr;
      }
    }

    if (url.length === 0 || password.length === 0 || username.length === 0 || abfrage === 0 ) {
      return null;
    } else {
      const c = {
    sewobeUrl : url,
    sewobePasswort: password,
    sewobeAbfrageNr: abfrage,
    sewobeUsername: username
      } as SewobeConfig;
      if (this.validate(c)) {
        return c;
      } else {
        return null;
      }
    }
     } catch (e) {
       return null;
     }
  }

  constructor() {
    this.lastConfig = this.parseConfigOrNull(localStorage.getItem(this.localStorageKey));
    if (this.lastConfig === null) {
      this.lastConfig = MOCK_CONFIGURATION;
    }
    this.config = new BehaviorSubject<SewobeConfig>(this.lastConfig);
   }

  public getConfig(): Observable<SewobeConfig> {
    return this.config;
  }


  public validate(config: SewobeConfig): boolean {
    return true; //TODO: error checking
  }

  public store(config: SewobeConfig): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(config));
    this.config.next(config);
  }

  public reemit(): void {
    this.config.next(this.lastConfig);
  }

}
