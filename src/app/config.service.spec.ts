/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConfigService, MOCK_CONFIGURATION } from './config.service';
import 'rxjs/Rx';

describe('ConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigService]
    });
  });

  it('should ...', inject([ConfigService], (service: ConfigService) => {
    expect(service).toBeTruthy();
  }));

  it('should parse wrong json as null', inject([ConfigService], (service: ConfigService) => {
    expect(service.parseConfigOrNull('{aaa:aaa}')).toBeNull();
  }));

  it('should parse right json as config', inject([ConfigService], (service: ConfigService) => {
    const config = MOCK_CONFIGURATION;
    const configStr = JSON.stringify(config);
    const result = service.parseConfigOrNull(configStr);
    expect(result.sewobeAbfrageNr === config.sewobeAbfrageNr).toBeTruthy();
    expect(result.sewobeUsername === config.sewobeUsername).toBeTruthy();
    expect(result.sewobePasswort === config.sewobePasswort).toBeTruthy();
    expect(result.sewobeUrl === config.sewobeUrl).toBeTruthy();
  }));
});
