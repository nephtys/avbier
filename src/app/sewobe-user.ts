export class SewobeUser {
    vorname: string;
    nachname: string;
    biername: string;
    mitgliedernummer: number;
    lastschrift: boolean;
    similarity: number;
}
