import { AvbierPage } from './app.po';

describe('avbier App', () => {
  let page: AvbierPage;

  beforeEach(() => {
    page = new AvbierPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
